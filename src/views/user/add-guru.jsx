import React from 'react';
import {
  Form,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
  Button
} from 'reactstrap';
import { NavLink } from 'react-router-dom';

const FormGuru = (props) => {
  return (
    <Form>
      <h6 className="heading-small text-muted mb-4">
        Informasi personal
      </h6>
      <Row>
        <Col>
          <FormGroup>
            <Label for="username" className="form-control-label">Username</Label>
            <Input id="username" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
        <Col>
          <FormGroup>
            <Label for="email" className="form-control-label">Email</Label>
            <Input id="email" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label for="nama_lengkap" className="form-control-label">Nama Lengkap</Label>
            <Input id="nama_lengkap" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label for="tempat_lahir" className="form-control-label">Tempat Lahir</Label>
            <Input id="tempat_lahir" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
        <Col>
          <FormGroup>
            <Label for="tanggal_lahir" className="form-control-label">Tanggal Lahir</Label>
            <Input id="tanggal_lahir" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label for="nama_ayah" className="form-control-label">Nama Ayah</Label>
            <Input id="nama_ayah" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
        <Col>
          <FormGroup>
            <Label for="nama_ibu" className="form-control-label">Nama Ibu</Label>
            <Input id="nama_ibu" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
      </Row>
      <hr className="my-4" />
      <h6 className="heading-small text-muted mb-4">
        Informasi kontak
      </h6>
      <Row>
        <Col lg="4">
          <FormGroup>
            <Label for="kota_asal" className="form-control-label">Kota Asal</Label>
            <Input id="kota_asal" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
        <Col lg="8">
          <FormGroup>
            <Label for="alamat" className="form-control-label">Alamat</Label>
            <Input id="alamat" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label for="no_telp" className="form-control-label">No. Telp</Label>
            <Input id="no_telp" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
        <Col>
          <FormGroup>
            <Label for="no_telp_ortu" className="form-control-label">No. Telp Orang Tua</Label>
            <Input id="no_telp_ortu" type="text" className="form-control-alternative" />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col className="text-right">
          <NavLink to="/a/user">
            <Button type="button" color="danger" className="mx-1">Cancel</Button>
          </NavLink>
          <Button color="info" className="mx-1">Submit</Button>
        </Col>
      </Row>
    </Form>
  )
}

export default FormGuru;