import React from 'react';
import { 
  Container, 
  Card, 
  CardHeader, 
  CardBody,
  Row,
  Col,
} from 'reactstrap';
import { NavLink } from 'react-router-dom';

const UserList = () => {
  return (
    <Container>
      <Card className="bg-secondary shadow">
        <CardHeader className="bg-white border-0">
          <Row className="align-items-center">
            <Col><h3 className="m-0">Users</h3></Col>
            <Col className="text-right">
              <NavLink className="btn btn-sm btn-info" to="/a/user/add">
                Tambah
                <i className="fas fa-plus ml-2"/>
              </NavLink>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <p>List User</p>
        </CardBody>
      </Card>
    </Container>
  )
}

export default UserList;