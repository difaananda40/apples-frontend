import React, { useState } from 'react';
import { 
  Container, 
  Card, 
  CardHeader, 
  CardBody,
  Row,
  Col,
  FormGroup,
  CustomInput
} from 'reactstrap';

import FormGuru from './add-guru';
import FormMurid from './add-murid';

const UserAdd = (props) => {
  const [ tipe, setTipe ] = useState('')
  const handleTipe = (e) => {
    const { value } = e.target;
    setTipe(value)
  }

  return (
    <Container>
      <Card className="bg-secondary shadow">
        <CardHeader className="bg-white border-0">
          <h3 className="m-0">Tambah User</h3>
        </CardHeader>
        <CardBody>
          <Row>
            <Col>
              <FormGroup>
                <h6 className="heading-small text-muted">
                  Pilih tipe user
                </h6>
                <CustomInput 
                  type="radio" 
                  selected={tipe}
                  value="guru" 
                  onChange={handleTipe} 
                  name="tipe_user" 
                  id="tipe_user_guru" 
                  label="Guru" inline 
                />
                <CustomInput 
                  type="radio" 
                  selected={tipe}
                  value="murid" 
                  onChange={handleTipe} 
                  name="tipe_user" 
                  id="tipe_user_murid" 
                  label="Murid" inline 
                />
              </FormGroup>
            </Col>
          </Row>
          {
            tipe === 'guru'
            ? <FormGuru />
            : tipe === 'murid' && <FormMurid />
          }
        </CardBody>
      </Card>
    </Container>
  )
}

export default UserAdd;