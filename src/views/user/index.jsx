import React from 'react';
import { Route } from 'react-router-dom';
import list from './list';
import add from './add';

const User = () => (
  <>
    <Route exact path="/a/user" component={list} />
    <Route path="/a/user/add" component={add} />
  </>
)

export default User;