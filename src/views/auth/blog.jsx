import React from 'react';
import {
  Container,
  Row,
  Col,
  Button,
  Card, CardBody,
} from 'reactstrap';
import 'assets/scss/blog.scss';

export default class Blog extends React.Component {
  render() {
    return (
      <div id="blog">
        <Container fluid className="blog-main-content px-5">
          <Row className="mb-5">
            <Col>
              <h1 className="display-3 text-center">Postingan Blog Terbaru</h1>
            </Col>
          </Row>
          <Row className="mb-5">
            <Col>
              <Card className="shadow rounded-lg">
                <CardBody>
                  <Row className="p-4">
                    <Col xs="12" md="12" lg="3" className="text-center text-lg-left mb-3">
                      <img src={require('assets/img/profile/bg.jpg')} alt="Blog Post" className="post-img" />
                    </Col>
                    <Col xs="12" md="12" lg="9" className="text-center text-lg-left">
                      <h2>Pendaftaran Tahun Baru Ajaran 2020</h2>
                      <p className="post-overview-content text-muted">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                      <p className="text-muted">Author: Difa Ananda (Mei, 2019)</p>
                      <Button color="primary">Read More...</Button>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row className="mb-5">
            <Col>
              <Card className="shadow rounded-lg">
                <CardBody>
                  <Row className="p-4">
                    <Col xs="12" md="12" lg="3" className="text-center text-lg-left mb-3">
                      <img src={require('assets/img/profile/bg.jpg')} alt="Blog Post" className="post-img" />
                    </Col>
                    <Col xs="12" md="12" lg="9" className="text-center text-lg-left">
                      <h2>Pendaftaran Tahun Baru Ajaran 2020</h2>
                      <p className="post-overview-content text-muted">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                      <p className="text-muted">Author: Difa Ananda (Mei, 2019)</p>
                      <Button color="primary">Read More...</Button>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row className="mb-5">
            <Col>
              <Card className="shadow rounded-lg">
                <CardBody>
                  <Row className="p-4">
                    <Col xs="12" md="12" lg="3" className="text-center text-lg-left mb-3">
                      <img src={require('assets/img/profile/bg.jpg')} alt="Blog Post" className="post-img" />
                    </Col>
                    <Col xs="12" md="12" lg="9" className="text-center text-lg-left">
                      <h2>Pendaftaran Tahun Baru Ajaran 2020</h2>
                      <p className="post-overview-content text-muted">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                      <p className="text-muted">Author: Difa Ananda (Mei, 2019)</p>
                      <Button color="primary">Read More...</Button>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}