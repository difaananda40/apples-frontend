import React from 'react';
import {
  Jumbotron,
  Container,
  Row,
  Col,
  Button,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText
} from 'reactstrap';
import 'assets/scss/home.scss';

export default class Home extends React.Component {
  render() {
    return (
      <div id="home">
        <Jumbotron fluid>
          <Container className="text-white">
            <h1 className="display-4">Belajar dan masuk ke PTN tujuan mu bersama kami.</h1>
          </Container>
        </Jumbotron>
        <Container>
          <h1 className="mb-4">Blog Posts</h1>
          <Row className="mb-5">
            <Col xs="12" md="12" lg="3" className="text-center text-lg-left mb-3">
              <img src={require('assets/img/profile/bg.jpg')} alt="Blog Post" className="post-img" />
            </Col>
            <Col xs="12" md="12" lg="9" className="text-center text-lg-left">
              <h2>Pendaftaran Tahun Baru Ajaran 2020</h2>
              <p className="post-overview-content text-muted">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
              <p className="text-muted">Author: Difa Ananda</p>
              <Button color="primary">Read More...</Button>
            </Col>
          </Row>
        </Container>
        <Container>
          <h1 className="mb-4">Reviews</h1>
          <Row className="justify-content-center">
            <Col className="mb-5" sm="12" md="6" lg="4">
              <Card className="shadow">
                <img src={require('assets/img/profile/bg.jpg')} alt="Review Card" className="rounded-circle review-img m-4 align-self-center" />
                <CardBody className="text-center">
                  <CardTitle className="text-uppercase">Muhammad Difa Ananda</CardTitle>
                  <CardSubtitle className="rating mb-2">
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                  </CardSubtitle>
                  <CardText className="text-muted font-secondary">
                    Pengajar sangat interaktif dan materi yang diajarkan tidak membosankan.
                  </CardText>
                </CardBody>
              </Card>
            </Col>
            <Col className="mb-5" sm="12" md="6" lg="4">
              <Card className="shadow">
                <img src={require('assets/img/profile/bg.jpg')} alt="Review Card" className="rounded-circle review-img m-4 align-self-center" />
                <CardBody className="text-center">
                  <CardTitle className="text-uppercase">Muhammad Difa Ananda</CardTitle>
                  <CardSubtitle className="rating mb-2">
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                  </CardSubtitle>
                  <CardText className="text-muted font-secondary">
                    Pengajar sangat interaktif dan materi yang diajarkan tidak membosankan.
                  </CardText>
                </CardBody>
              </Card>
            </Col>
            <Col className="mb-5" sm="12" md="6" lg="4">
              <Card className="shadow">
                <img src={require('assets/img/profile/bg.jpg')} alt="Review Card" className="rounded-circle review-img m-4 align-self-center" />
                <CardBody className="text-center">
                  <CardTitle className="text-uppercase">Muhammad Difa Ananda</CardTitle>
                  <CardSubtitle className="rating mb-2">
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                    <i className="fas fa-star"></i>
                  </CardSubtitle>
                  <CardText className="text-muted font-secondary">
                    Pengajar sangat interaktif dan materi yang diajarkan tidak membosankan.
                  </CardText>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}