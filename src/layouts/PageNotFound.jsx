import React from 'react';

const PageNotFound = () => (
  <p>404 Not Found</p>
);

export default PageNotFound;