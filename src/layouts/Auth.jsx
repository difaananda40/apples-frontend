import React from "react";
import { Route, Switch, NavLink, Redirect } from "react-router-dom";

// import {
//   Container,
//   Row,
//   Col,
//   Collapse,
//   Navbar,
//   NavbarToggler,
//   NavbarBrand,
//   Nav,
//   NavItem,
//   UncontrolledDropdown,
//   DropdownToggle,
//   DropdownMenu,
//   DropdownItem } from 'reactstrap';

// core components
import AuthNavbar from "components/Navbars/AuthNavbar.jsx";
// import AuthFooter from "components/Footers/AuthFooter.jsx";

import routes from "routes.js";

import PageNotFound from "layouts/PageNotFound.jsx";

class Auth extends React.Component {
  state = {
    isOpen: false
  };
  
  toggle = this.toggle.bind(this);

  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/p") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    const path = this.props.location.pathname;
    return (
      <>
        <AuthNavbar path={path}/>
        <Switch>
          {this.getRoutes(routes)}
          <Redirect from="/p" to="/p/home" />
        </Switch>
        {/* <AuthFooter /> */}
      </>
    );
  }
}

export default Auth;
