/*!

=========================================================
* Argon Dashboard React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {
  UncontrolledCollapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col
} from "reactstrap";

import 'assets/scss/navbar.scss';

class AdminNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      navbar: false,
    }
  }

  componentDidMount() {
    window.addEventListener("scroll", () => this.handleScroll())
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", () => this.handleScroll())
  }

  handleScroll = () => {
    const currentScrollPos = window.pageYOffset;
    const navbar = currentScrollPos > 0;
    this.setState({
      navbar
    })
  }

  render() {
    const path = this.props.path;
    return (
      <>
        <Navbar
          className='navbar-top fixed-top navbar-horizontal navbar-dark'
          expand="md"
          color={this.state.navbar || path !== '/p/home' ? 'dark' : 'transparent'}
          style={{transition: 'background-color .2s ease-in-out'}}
        >
          <Container className="px-4">
            <NavbarBrand to="/p/home" tag={Link}>
              <img alt="..." src={require("assets/img/brand/argon-react-white2.png")} />
            </NavbarBrand>
            <button className="navbar-toggler" id="navbar-collapse-main">
              <span className="navbar-toggler-icon" />
            </button>
            <UncontrolledCollapse navbar toggler="#navbar-collapse-main">
              <div className="navbar-collapse-header d-md-none">
                <Row>
                  <Col className="collapse-brand" xs="6">
                    <Link to="/">
                      <img
                        alt="..."
                        src={require("assets/img/brand/argon-react2.png")}
                      />
                    </Link>
                  </Col>
                  <Col className="collapse-close" xs="6">
                    <button
                      className="navbar-toggler"
                      id="navbar-collapse-main"
                    >
                      <span />
                      <span />
                    </button>
                  </Col>
                </Row>
              </div>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink className={`nav-link-icon ${path === '/p/home' && 'text-info'}`} to="/p/home" tag={Link}>
                    <span className="nav-link-inner--text">Home</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className={`nav-link-icon ${path === '/p/blog' && 'text-info'}`} to="/p/blog" tag={Link}>
                    <span className="nav-link-inner--text">Blog</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className={`nav-link-icon`} to="/a/index" tag={Link}>
                    <span className="nav-link-inner--text">Member Area</span>
                  </NavLink>
                </NavItem>
              </Nav>
            </UncontrolledCollapse>
          </Container>
        </Navbar>
      </>
    );
  }
}

export default AdminNavbar;
