/*!

=========================================================
* Argon Dashboard React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Index from "views/Index.jsx";
import Icons from "views/examples/Icons.jsx";
import Home from "views/auth/home.jsx";
import Blog from "views/auth/blog.jsx";
import User from "views/user/index.jsx";

var routes = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-tv-2 text-primary",
    component: Index,
    layout: "/a"
  },
  {
    path: "/icons",
    name: "Icons",
    icon: "ni ni-planet text-blue",
    component: Icons,
    layout: "/a"
  },
  {
    path: "/user",
    name: "User",
    icon: "fas fa-user text-blue",
    component: User,
    layout: "/a",
    role: "admin"
  },
  {
    path: "/home",
    name: "Home",
    icon: "ni ni-key-25 text-info",
    component: Home,
    layout: "/p"
  },
  {
    path: "/blog",
    name: "Blog",
    icon: "ni ni-key-25 text-info",
    component: Blog,
    layout: "/p"
  },
];
export default routes;
